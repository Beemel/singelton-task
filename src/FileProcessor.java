import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileProcessor {
    private String data;
    private static FileProcessor instance;

    private FileProcessor (String data){
        this.data = data;
    }

    public static FileProcessor getInstance(String data) {
        instance = new FileProcessor (data);

        Path filePath = Paths.get("C:/Reskilling2022/Java/projects/SingletonPattern/src", data);

        try (Stream<String> lines = Files.lines( filePath ))
        {
            lines.forEach(System.out::println);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}


